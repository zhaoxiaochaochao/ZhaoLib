//
//  BaseLibDefine.h
//  CommonBaseLib
//
//  Created by Chaim Chen on 2018/9/28.
//  Copyright © 2018年 zyb. All rights reserved.
//

#ifndef BaseLibDefine_h
#define BaseLibDefine_h

#define DeviceIsPad       (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define DeviceIsPhone     (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)

#define IOS_2_0 @"2.0"
#define IOS_3_0 @"3.0"
#define IOS_4_0 @"4.0"
#define IOS_5_0 @"5.0"
#define IOS_6_0 @"6.0"
#define IOS_7_0 @"7.0"
#define IOS_8_0 @"8.0"
#define IOS_9_0 @"9.0"
#define IOS_10_0 @"10.0"
#define IOS_11_0 @"11.0"

#define ReplaceNaN(number) (number = isnan(number) ? MAXFLOAT : number)

#pragma mark - System Version
#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)

#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)

#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)

#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)

#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)

#endif /* BaseLibDefine_h */
