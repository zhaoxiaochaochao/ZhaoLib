# ZhaoLib

[![CI Status](https://img.shields.io/travis/18263346783/ZhaoLib.svg?style=flat)](https://travis-ci.org/18263346783/ZhaoLib)
[![Version](https://img.shields.io/cocoapods/v/ZhaoLib.svg?style=flat)](https://cocoapods.org/pods/ZhaoLib)
[![License](https://img.shields.io/cocoapods/l/ZhaoLib.svg?style=flat)](https://cocoapods.org/pods/ZhaoLib)
[![Platform](https://img.shields.io/cocoapods/p/ZhaoLib.svg?style=flat)](https://cocoapods.org/pods/ZhaoLib)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

ZhaoLib is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'ZhaoLib'
```

## Author

18263346783, zhaowenchao@zuoyebang.com

## License

ZhaoLib is available under the MIT license. See the LICENSE file for more info.
